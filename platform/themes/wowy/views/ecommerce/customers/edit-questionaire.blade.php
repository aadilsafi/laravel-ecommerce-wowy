@extends(Theme::getThemeNamespace() . '::views.ecommerce.customers.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{ __('Questionaire') }}</h5>
        </div>
        <div class="card-body">
            {!! Form::open(['route' => 'customer.edit-questionaire']) !!}
                <div class="row">
                    <div class="form-group col-md-12 @if ($errors->has('purpose')) has-error @endif">
                        <label class="required" for="purpose">{{ __('For what purpose are you buying a laptop/smartphone') }}:</label>
                        <input required class="form-control square" name="purpose" type="text" id="purpose" value="{{ auth('customer')->user()->purpose }}">
                        {!! Form::error('name', $errors) !!}
                    </div>
                    <div class="form-group col-md-12 @if ($errors->has('laptop_budget')) has-error @endif">
                        <label for="laptop_budget">{{ __('What is your budget for a laptop?') }}:</label>
                        <input id="laptop_budget" type="text" class="form-control square" name="laptop_budget" placeholder="Laptop Budget" value="{{ auth('customer')->user()->laptop_budget }}">
                        {!! Form::error('name', $errors) !!}
                    </div>
                    {{-- <div class="form-group col-md-12">
                        <label for="phone_budget">{{ __('phone_budget') }}:</label>
                        <input id="phone_budget" type="text" class="form-control" disabled="disabled" value="{{ auth('customer')->user()->phone_budget }}" name="phone_budget">
                    </div> --}}
                    <div class="form-group col-md-12 @if ($errors->has('phone')) has-error @endif">
                        <label for="phone_budget">{{ __('Phone Budget') }}:</label>
                        <input type="text" class="form-control square" name="phone_budget" id="phone_budget" placeholder="{{ __('Phone Budget') }}" value="{{ auth('customer')->user()->phone_budget }}">
                        {!! Form::error('name', $errors) !!}
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-fill-out submit">{{ __('Update') }}</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
